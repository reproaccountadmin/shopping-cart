package shopping.processing.database;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import shopping.entity.eo.FPSCustomerOrder;
import shopping.entity.eo.FPSFreePlusShippingAddress;
import shopping.entity.eo.FPSPerson;
import shopping.utility.date.DateUtility;


public class QueryDatabase {

	private static Logger log = Logger.getLogger(QueryDatabase.class);

	
	private QueryDatabase() {}

	
	public static NSArray<FPSCustomerOrder> FPSCustomerOrdersForDay(NSTimestamp day) throws Exception {
		
		EOEditingContext _ec = ERXEC.newEditingContext();
		
		NSTimestamp firstSecondStartOfDay = DateUtility.OneSecondPastMidnightOfToday(day);
		NSTimestamp lastSecondStartOfDay = DateUtility.LastSecondOfDayToday(day);
		NSArray<NSTimestamp> args = new NSArray<NSTimestamp>(new NSTimestamp[]{firstSecondStartOfDay, lastSecondStartOfDay});
		EOQualifier myQual = EOQualifier.qualifierWithQualifierFormat("(date >= %@ and date <= %@) ", args);

		log.info("Qualifier " + myQual);

		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey("date", EOSortOrdering.CompareAscending);
		NSArray<EOSortOrdering> orderings = new NSArray<EOSortOrdering>(order);
					
		return (NSArray<FPSCustomerOrder>)FPSCustomerOrder.fetchFPSCustomerOrders(_ec, myQual, orderings);
	}
	
	
	public static NSArray<NSDictionary<Object, Object>> ArrayForOrdersPersonAndLocation(NSTimestamp date) throws Exception {
		
		NSArray<FPSCustomerOrder> orders = QueryDatabase.FPSCustomerOrdersForDay(date);
		
		NSMutableArray<NSDictionary<Object, Object>> notifications = new NSMutableArray<NSDictionary<Object, Object>>();
		
		if(orders.count() > 0){
			
			for(FPSCustomerOrder order : orders) {
				
				FPSPerson person = order.fpsPerson();
				FPSFreePlusShippingAddress address = person.fpsShippingAddress();
				
				NSMutableDictionary<Object, Object> aNotification = new NSMutableDictionary<Object, Object>();
				
				String _customer = person.fullName();
				String _location = (address.city() + ", " + address.state());
				String _order = order.lineOrder();
				
				aNotification.setObjectForKey(_customer, CustomerConstants.NOTIFICATION_CUSTOMER);
				aNotification.setObjectForKey(_location, CustomerConstants.NOTIFICATION_LOCATION);
				aNotification.setObjectForKey(_order, CustomerConstants.NOTIFICATION_ORDER);
				notifications.addObject(aNotification.immutableClone());
				
			}
			
			return notifications.immutableClone();
		}
		
		
		return (NSArray.emptyArray());
		
	}
	
	
	
	
	
	
	
}
