package shopping.processing.database;

import com.webobjects.foundation.NSArray;

public final class CustomerConstants {
	
	private CustomerConstants(){}

	public static final String FULL_NAME = "fullName";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL = "emailAddress";
	public static final String FULL_ADDRESS = "addressOne";
	public static final String CITY_NAME = "city";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipcode";
	public static final String SHIPPING_COUNTRY = "country";
	
	public static final String FREE_OFFER_ID = "free-offer-id";
	
	
	public static final String UNITED_STATES = "United States of America";
	
	public static final String COST = "cost";
	public static final String ITEM = "item";
	public static final String TOKEN = "token";
	public static final String STRIPE_TOKEN = "stripeToken";
	public static final String SOURCE = "source";
	public static final String SHIPPING = "shipping";


	
	public static final String PENDING = "pending";
	public static final String FAIL = "failed";
	public static final String COMPLETE = "complete";
	
	public static final String MY_BUDDI = "mybuddi";
	
	
	public static final String SUBJECT_PURCHASE_INVOICE = "My Buddi - Congratulations you did it!";
	
	
	public static final String NOTIFICATION_CUSTOMER = "customer";
	public static final String NOTIFICATION_LOCATION = "location";
	public static final String NOTIFICATION_ORDER = "order";



	public static NSArray<String> KEYS() {
		NSArray<String> keys = new NSArray<String>(new String[] { CustomerConstants.FULL_NAME, CustomerConstants.EMAIL, CustomerConstants.FULL_ADDRESS, CustomerConstants.CITY_NAME, CustomerConstants.STATE, CustomerConstants.ZIPCODE, CustomerConstants.SHIPPING_COUNTRY, CustomerConstants.FREE_OFFER_ID }) ;
		return keys;
	}
	
	
}
