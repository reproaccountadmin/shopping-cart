package shopping.processing.database;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;
import shopping.entity.eo.FPSChargeDetail;
import shopping.entity.eo.FPSCustomerOrder;
import shopping.entity.eo.FPSDateJoined;
import shopping.entity.eo.FPSPerson;
import shopping.utility.date.DateUtility;
import shopping.entity.eo.FPSFreePlusShippingAddress;
import shopping.entity.eo.FPSInvoice;
import shopping.validate.CountryCode;

public final class EnterpriseObjectsManagement {
   private static Logger log = Logger.getLogger(EnterpriseObjectsManagement.class);

	private EnterpriseObjectsManagement(){}
	
	
	
	
	/* Invoice Number */
	
	
    public static synchronized String InvoiceNumber()
    {           
    	
    	
    	String invoiceNumber = "";
    	EOEditingContext _ec = ERXEC.newEditingContext();
     	
    	_ec.lock();
        try{
            FPSInvoice item = (FPSInvoice)EOUtilities.objectWithPrimaryKeyValue(_ec, "FPSInvoice", new Integer("1"));
            
            int number = new Integer(item.invoiceNumber()).intValue();
            number++;
            
            invoiceNumber = (DateUtility.StringForCurrentYearLastTwoDigits())+"MB" + new Integer((number + 100000)).toString();
            
            item.setInvoiceNumber(new Integer(number).toString());
            
       
            	_ec.saveChanges();
            }catch (Exception e) {
                
            }finally {
            	_ec.unlock();
            }
            
        return invoiceNumber;
    	
    }
	
	
    /* FPS Customer Order */
    
    
	//EOEditingContext _ec = ERXEC.newEditingContext();
	public static FPSCustomerOrder CreateCustomerOrder( EOEditingContext _ec ) throws NSValidation.ValidationException{
		
		_ec.lock();
		
		try {
		
			FPSCustomerOrder customerOrder = FPSCustomerOrder.createFPSCustomerOrder(_ec);

			
			return customerOrder;
			
		}catch(Exception e){
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}
		
	}
	
	public static FPSCustomerOrder CreateCustomerOrderWithDetails( EOEditingContext _ec, String filePath,  String subTotal, String shipping, String total, String item  ) throws NSValidation.ValidationException{
		
		FPSCustomerOrder customerOrder = EnterpriseObjectsManagement.CreateCustomerOrder(_ec);
		String invoiceNumber = EnterpriseObjectsManagement.InvoiceNumber();
		customerOrder.setDate(new NSTimestamp());
		customerOrder.setInvoiceUrl(filePath);
		customerOrder.setInvoiceNumber(invoiceNumber);
		customerOrder.setSubTotal(subTotal);
		customerOrder.setShipping(shipping);
		customerOrder.setTotal(total);
		customerOrder.setLineOrder(item);

		
		return customerOrder;
	}

	
    
    
	
	/* Charge FPSChargeDetail */
	
	//EOEditingContext _ec = ERXEC.newEditingContext();
	private static FPSChargeDetail CreateCharge( EOEditingContext _ec ) throws NSValidation.ValidationException{
		
		_ec.lock();
		
		try {
		
			FPSChargeDetail chargeDetail = FPSChargeDetail.createFPSChargeDetail(_ec);
			chargeDetail.setPaymentStatus(CustomerConstants.PENDING);
			chargeDetail.setIsPaymentComplete(false);
			chargeDetail.setAmount(BigDecimal.ZERO);
			
			return chargeDetail;
			
		}catch(Exception e){
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}
		
	}
	
	public static FPSChargeDetail CreateChargeDetails(EOEditingContext _ec, EOGlobalID globalId, NSDictionary<Object, Object> result) throws NumberFormatException, NSValidation.ValidationException{
		
		_ec.lock();
			try {
			FPSPerson person = (FPSPerson) _ec.faultForGlobalID(globalId, _ec);
			FPSChargeDetail chargeDetail = CreateCharge(_ec);
			chargeDetail.setFpsPerson(person);
			
			String _amount_total = (String)result.objectForKey(CustomerConstants.COST);
			String _item = (String)result.objectForKey(CustomerConstants.ITEM);
	
			Double amount = (new Double(_amount_total) / 100);
			BigDecimal amountDecimal = new BigDecimal(amount).setScale(2, BigDecimal.ROUND_DOWN);
	
			chargeDetail.setAmount(amountDecimal);
			chargeDetail.setItem(_item);
			
			chargeDetail.validateForSave();
			return chargeDetail;
		
		}catch(Exception e){
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}
		
		
		
	}

	public static FPSChargeDetail UpdateChargeDetails( EOEditingContext _ec, EOGlobalID globalId, NSDictionary<Object, Object> result) throws NumberFormatException, NSValidation.ValidationException{
	
		_ec.lock();
		
		log.info("EOGlobal ID " + globalId);
		
		try {
			FPSChargeDetail fpsChargeDetails = (FPSChargeDetail) _ec.faultForGlobalID(globalId, _ec);
			
			String _amount_total = (String)result.objectForKey(CustomerConstants.COST);
			String _item = (String)result.objectForKey(CustomerConstants.ITEM);
		
			Double amount = (new Double(_amount_total) / 100);
			BigDecimal amountDecimal = new BigDecimal(amount).setScale(2, BigDecimal.ROUND_DOWN);
		
			fpsChargeDetails.setAmount(amountDecimal);
			fpsChargeDetails.setItem(_item);
				
	
			fpsChargeDetails.validateForSave();
		
			return fpsChargeDetails;

		}catch(Exception e){
			e.printStackTrace();
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}		
	}

/* END Charge FPSChargeDetail */
	
	
/* FPSFreePlusShippingAddressl */

	private static FPSFreePlusShippingAddress CreateFreePlusShippingAddress(NSDictionary<String, String> personDictionary, EOEditingContext _ec) throws NSValidation.ValidationException {
		
		_ec.lock();
		
		try {
		
			FPSFreePlusShippingAddress freePlusShippingAddress = FPSFreePlusShippingAddress.createFPSFreePlusShippingAddress(_ec);
			freePlusShippingAddress.setAddressOne(personDictionary.objectForKey(FPSFreePlusShippingAddress.ADDRESS_ONE_KEY));
			freePlusShippingAddress.setCity(personDictionary.objectForKey(FPSFreePlusShippingAddress.CITY_KEY));
			freePlusShippingAddress.setState(personDictionary.objectForKey(FPSFreePlusShippingAddress.STATE_KEY));
			freePlusShippingAddress.setZipcode(personDictionary.objectForKey(FPSFreePlusShippingAddress.ZIPCODE_KEY));
			freePlusShippingAddress.setCountry(personDictionary.objectForKey(FPSFreePlusShippingAddress.COUNTRY_KEY));
			return freePlusShippingAddress;
		
		}catch(Exception e) {
			
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		
		}finally {
			
			_ec.unlock();
		
		}	
	}
	
	

	
	public static FPSPerson UpdateShippingInformation(NSDictionary<String, String> personDictionary, EOEditingContext _ec, String email) throws NSValidation.ValidationException{

		_ec.lock();
		try {
			
			
		
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("emailAddress = " + "\'" + email + "\'", null);
			FPSPerson person = FPSPerson.fetchFPSPerson(_ec, qual);
			
			FPSFreePlusShippingAddress freePlusShippingAddress = person.fpsShippingAddress();
			freePlusShippingAddress.setAddressOne(personDictionary.objectForKey(FPSFreePlusShippingAddress.ADDRESS_ONE_KEY));
			freePlusShippingAddress.setCity(personDictionary.objectForKey(FPSFreePlusShippingAddress.CITY_KEY));
			freePlusShippingAddress.setState(personDictionary.objectForKey(FPSFreePlusShippingAddress.STATE_KEY));
			freePlusShippingAddress.setZipcode(personDictionary.objectForKey(FPSFreePlusShippingAddress.ZIPCODE_KEY));
			freePlusShippingAddress.setCountry(personDictionary.objectForKey(FPSFreePlusShippingAddress.COUNTRY_KEY));
			
			if(freePlusShippingAddress.country().equals(CustomerConstants.UNITED_STATES)){
				freePlusShippingAddress.setCountryCode(CountryCode.USA);
			}
			

	
			freePlusShippingAddress.validateForSave();
			
			_ec.saveChanges();
		
			return person;
		
		
		}catch(Exception e){
		
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		
		}finally{
			_ec.unlock();
		}
		

		
	}
	
	
	
/* FPSFreePlusShippingAddressl */

	
	
	/* FPSDateJoined */
	
	private static FPSDateJoined CreateDateJoined(EOEditingContext _ec) throws NSValidation.ValidationException{
		
		_ec.lock();
		try {
		
			FPSDateJoined dateJoined = FPSDateJoined.createFPSDateJoined(_ec);
			dateJoined.setTimeStamp(new NSTimestamp());
			dateJoined.setIsActive(false);
			return dateJoined;
		
		}catch(Exception e){
			
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		
		}finally{
			_ec.unlock();
		}
		
	}
	/* FPSDateJoined */	
	
	
	/* FPSPerson */	
	
	private static FPSPerson CreatePerson(NSDictionary<String, String> personDictionary, EOEditingContext _ec)throws NSValidation.ValidationException{
		
		
		_ec.lock();
		try {
			
			FPSPerson person = FPSPerson.createFPSPerson(_ec);
			
			person.setFullName(personDictionary.objectForKey(FPSPerson.FULL_NAME_KEY));
			person.setEmailAddress(personDictionary.objectForKey(FPSPerson.EMAIL_ADDRESS_KEY));
			
			person.setFirstName(personDictionary.objectForKey(FPSPerson.FIRST_NAME_KEY));
			person.setLastName(personDictionary.objectForKey(FPSPerson.LAST_NAME_KEY));
			
			return person;
		
		}catch(Exception e){
			
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		
		}finally{
			_ec.unlock();
		}
	}
	
	public static FPSPerson InsertPerson(NSDictionary<String, String> personDictionary, EOEditingContext _ec) throws NSValidation.ValidationException{
		
		
		FPSPerson person = CreatePerson(personDictionary, _ec);		
		FPSDateJoined dateJoined = CreateDateJoined(_ec);
		FPSFreePlusShippingAddress freePlusShippingAddress = CreateFreePlusShippingAddress(personDictionary, _ec);
	//	ChargeDetail chargeDetail = CreateCharge(_ec);

		

		
		if(freePlusShippingAddress.country().equals(CustomerConstants.UNITED_STATES)){
			freePlusShippingAddress.setCountryCode(CountryCode.USA);
		}else {
			freePlusShippingAddress.setCountryCode(CountryCode.NOT_USA);
		}
		
		_ec.lock();
		try {
			person.validateForSave();
			dateJoined.validateForSave();
			freePlusShippingAddress.validateForSave();
			
			//chargeDetail.validateForSave();
		}catch(Exception e){
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}
		
		
		_ec.lock();
		try {
			_ec.saveChanges();
			person.setFpsShippingAddress(freePlusShippingAddress);
			person.setFpsDateJoined(dateJoined);
			freePlusShippingAddress.setFpsPerson(person);
			//chargeDetail.setPerson(person);

			_ec.saveChanges();
			
		}catch(Exception e){
			throw new NSValidation.ValidationException(e.getMessage());

		}finally{
			_ec.unlock();
		}
		return person;

	}
	
	/* FPSPerson */	
	
	/* SAVE EOs */	

	public static void SaveAllEnterpriseObjects(EOEditingContext _ec) throws NSValidation.ValidationException {
		
		_ec.lock();
		try {
			_ec.saveChanges();
		
		}catch(Exception e){
			_ec.revert();
			throw new NSValidation.ValidationException(e.getMessage());

		}finally{
			_ec.unlock();
		}
	}
	
}
