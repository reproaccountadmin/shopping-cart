package shopping.processing.database;

import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import billing.payment.constant.Constants;
import billing.payment.order.CustomerOrder;
import shopping.entity.eo.FPSFreePlusShippingAddress;
import shopping.entity.eo.FPSPerson;
import shopping.processing.app.DirectAction;
import shopping.utility.date.DateUtility;

public class DecodeCustomerFormResults {

    private static Logger log = Logger.getLogger(DecodeCustomerFormResults.class);

	
	private NSDictionary<String, NSArray<Object>> _formValues;
	
	public DecodeCustomerFormResults(NSDictionary<String, NSArray<Object>> formValues){
		super();
		this._formValues = formValues;
	}
	
	
	private NSDictionary<String, NSArray<Object>> formValues(){
		return _formValues;
	}
	
	public static String ItemWithSpacesFromUnderscore(String underscrored){
		
		String spaced = "";
		StringTokenizer tokens = new StringTokenizer(underscrored, "_");
		
		while(tokens.hasMoreTokens()){
			spaced += (" " + tokens.nextToken());
		}
		
		return spaced.trim();
	}
	
	
	public static NSMutableDictionary<Object, Object> CustomerDictionaryFromOrderDetails(NSMutableDictionary<Object, Object> order, String subTotal, String shipping, String total){
		
		String invoiceNumber = EnterpriseObjectsManagement.InvoiceNumber();

		order.setObjectForKey(invoiceNumber, Constants.INVOICE_NUMBER);
		order.setObjectForKey(shipping, Constants.SHIPPING);
		order.setObjectForKey(subTotal, Constants.SUB_TOTAL);
		order.setObjectForKey(total, Constants.TOTAL);
 		order.setObjectForKey("My Buddi is a trading name used in conjunction with My Buddi Apparel and Accessories", Constants.STRAP_LINE);
 		order.setObjectForKey("Base Enterprise US LLC", Constants.STATEMENT_REF);

		return order;
		
	}

	
	
public static NSMutableDictionary<Object, Object> CustomerDictionaryFromPersonAndShipping(EOEditingContext _ec, EOGlobalID globalId){
		
		
		
		_ec.lock();
		try{
			
			NSMutableDictionary<Object, Object> customerDictionary = new NSMutableDictionary<Object, Object>();
			
			FPSPerson person = (FPSPerson) _ec.faultForGlobalID(globalId, _ec);			
			FPSFreePlusShippingAddress address = person.fpsShippingAddress();
			
			String fullname = person.fullName() != null ? person.fullName() : " ";
			String addressOne = address.addressOne() != null ? address.addressOne() : " ";
			String addressTwo = address.addressTwo() != null ? address.addressTwo() : " ";
			String city = address.city() != null ? address.city() : " ";
			String state = address.state() != null ? address.state() : " ";
			String zipcode = address.zipcode() != null ? address.zipcode() : " ";
			String country = address.country() != null ? address.country() : " ";
			
			customerDictionary.setObjectForKey(fullname, Constants.FULLNAME);
			customerDictionary.setObjectForKey(addressOne, Constants.ADDRESS_ONE);
			customerDictionary.setObjectForKey(addressTwo, Constants.ADDRESS_TWO);
			customerDictionary.setObjectForKey(city, Constants.CITY);
			customerDictionary.setObjectForKey(state, Constants.STATE);
			customerDictionary.setObjectForKey(zipcode, Constants.ZIPCODE);
			customerDictionary.setObjectForKey(country, Constants.COUNTRY);

		
			return customerDictionary;
		
		}catch(Exception e){
			
			throw new NSValidation.ValidationException(e.getMessage());
		}finally{
			_ec.unlock();
		}
	}
	
	
	
	
	public static NSDictionary<Object, Object> StripeDictionary(NSDictionary<Object, Object> result, String stripeSecret) throws Exception {
		
		
		NSMutableDictionary<Object, Object> _dictionary = new NSMutableDictionary<Object, Object>();
		
		_dictionary.setObjectForKey((String)result.objectForKey(CustomerConstants.TOKEN), "source"); // token from website
		_dictionary.setObjectForKey(new Integer((String)result.objectForKey(CustomerConstants.COST)), "amount"); // amount in cents stored in session
		_dictionary.setObjectForKey("usd", "currency"); // currency hard coded for now
		_dictionary.setObjectForKey(stripeSecret, "apiKey"); // obtained from secure store and not hard coded
		_dictionary.setObjectForKey((String)result.objectForKey("item"), "description"); // obtained from secure store and not hard coded
		
		return _dictionary.immutableClone();
		
	}
	

	

	public NSDictionary<String, String> decodedFormDictionary() throws Exception {
		
		NSDictionary<String, NSArray<Object>> tempDict = (NSDictionary<String,NSArray<Object>>)this.formValues().immutableClone();
		
		
		NSMutableDictionary<String, String> dictionary = new NSMutableDictionary<String, String>();
		NSArray<String> keys = CustomerConstants.KEYS();
		String fullName = "";
		for (String key : keys) {
			String val = "";
			NSArray<Object> value = (NSArray<Object>)tempDict.objectForKey(key);
		
			
			if(value.count() < 1){
				val = "NOT SET";
			}else{
				val = (String) value.lastObject();
				if(key.equals(CustomerConstants.FULL_NAME)){
					fullName = val;
				}
			}
			dictionary.setObjectForKey(val, key);
		}
		
		if(!fullName.equals("")){
			NSArray<String> names = FirstAndLastName(fullName);
			
			if(names.count() == 1)throw new Exception("Please enter your first and lastname separated by a space.");
			dictionary.setObjectForKey(names.get(0), CustomerConstants.FIRST_NAME);
			dictionary.setObjectForKey(names.get(1), CustomerConstants.LAST_NAME);

		}
		
		return dictionary.immutableClone();
	}
	
	
	private static NSArray<String> FirstAndLastName(String fullName){
		
		NSMutableArray<String> firstAndLast = new NSMutableArray<String>();
		
		fullName = fullName.trim();
		
			StringTokenizer tokenizer = new StringTokenizer(fullName, " ");
			
				if(tokenizer != null && tokenizer.countTokens() >= 1){
					
					log.info("Tokenizer Count " + tokenizer.countTokens());
					if(tokenizer.countTokens() == 1){
						
						firstAndLast.add((String)tokenizer.nextElement());
						log.info("Only 1 token");

						//firstAndLast.add("Last Name");
					} else if(tokenizer.countTokens() == 2){
						
						log.info("2 tokens");
						
						while(tokenizer.hasMoreTokens()){
							firstAndLast.add((String)tokenizer.nextElement());
						}
						
					} else {
						log.info("more that 2 tokens");
						int i = 0;
						String lastName = "";
						
						while(tokenizer.hasMoreTokens()){
							
							if(i == 0){
								firstAndLast.add((String)tokenizer.nextElement());
							}else {
								lastName += ((String)tokenizer.nextElement() + " ");
							}
							
							i++;
						}
						
						firstAndLast.add(lastName.trim());
						
					}
				}else {
					firstAndLast.add("No First Name");
					firstAndLast.add("No Last Name");
				}
				
		return firstAndLast.immutableClone();
	}
	
}
