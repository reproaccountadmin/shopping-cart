package shopping.processing.json;

public class CredentialsJSON {

	private String sellerId;
	private String mwsAccessKey;
	private String mwsSecretKey;
	private String lwsClientId;
	private String clientSecret;
	private String stripeSecret;
	
	public CredentialsJSON() {
		// TODO Auto-generated constructor stub
	}

	public String getSellerId() {
		return this.sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getMwsAccessKey() {
		return this.mwsAccessKey;
	}

	public void setMwsAccessKey(String mwsAccessKey) {
		this.mwsAccessKey = mwsAccessKey;
	}

	public String getMwsSecretKey() {
		return this.mwsSecretKey;
	}

	public void setMwsSecretKey(String mwsSecretKey) {
		this.mwsSecretKey = mwsSecretKey;
	}

	public String getLwsClientId() {
		return this.lwsClientId;
	}

	public void setLwsClientId(String lwsClientId) {
		this.lwsClientId = lwsClientId;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getStripeSecret() {
		return stripeSecret;
	}

	public void setStripeSecret(String stripeSecret) {
		this.stripeSecret = stripeSecret;
	}

}


