package shopping.processing.response.json;


import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public class JSONHelper {

	
	
	
	private final static String PROTOCOL = "http";
	private final static String DOMAIN = "mybuddi.co";

	/*
	 * html response section
	 */
	
	public static WOResponse htmlResponseWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "html", false);
	}
	
	/*
	 * save as above with status code of 400 error
	 */
	
	public static WOResponse htmlResponseErrorWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "html", true);
	}
	
	/*
	 * script response
	 */
	
	public static WOResponse scriptResponseWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "script", false);
	}
	
	/*
	 * save as above with status code of 400 error
	 */
	
	public static WOResponse scriptResponseErrorWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "script", true);
	}
	
	
	/*
	 * json response section
	 */
	
	public static WOResponse jsonResponseWithContentString(String contentStr, WOResponse r) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "json", false, r);
	}
	
	public static WOResponse jsonResponseWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "json", false);
	}
	

	
	/*
	 * save as above with status code of 400 error
	 */
	
	public static WOResponse jsonResponseErrorWithContentString(String contentStr) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContent(content, "json", true);
	}
	
	/*
	 * common methods
	 */
	
	public static WOResponse jsonResponseWithContentStringAndCookie(String contentStr, NSArray<WOCookie> cookies) {
		NSData content = new NSData(contentStr, "UTF-8");
		return responseWithContentAndCookies(content, "json", false, cookies);
	}

	
	
	public static WOResponse responseWithContentAndCookies(NSData content, String contentType, boolean error, NSArray<WOCookie> cookies) {
	
		
		WOResponse response = new WOResponse();
	
		response.setContent(content);
		response.setContentEncoding("UTF8");
		response.setHeader("application/" + contentType + "; charset=UTF-8" , "Content-Type");
		response.setHeader("Connection", "keep-alive");
	    response.setHeader(Integer.toString(content.length()), "content-length");
	    response.setHeader(PROTOCOL +"://" + DOMAIN, "Access-Control-Allow-Origin"); // commented out for devel
	    response.setHeader("X-Requested-With", "Access-Control-Allow-Headers");
	    response.setHeader("true", "Access-Control-Allow-Credentials");
		response.setHeader("no-cache","Pragma");
		response.setHeader("max-age=0, no-cache, no-store","Cache-Control");
	
		for(WOCookie cookie:  cookies){
			response.addCookie(cookie);
		}
		
		
		if(error){
			response.setHeader("400", "status-code");
		}
		
		 System.out.println("here");
		return response;
	}
	
	
	public static WOResponse responseWithContent(NSData content, String contentType, boolean error) {
		WOResponse response = new WOResponse();
		response.setContent(content);
		response.setContentEncoding("UTF8");
		response.setHeader("application/" + contentType + "; charset=UTF-8" , "Content-Type");
		response.setHeader("Connection", "keep-alive");
	    response.setHeader(Integer.toString(content.length()), "content-length");
	    response.setHeader(PROTOCOL +"://" + DOMAIN, "Access-Control-Allow-Origin"); // commented out for devel
	    response.setHeader("X-Requested-With", "Access-Control-Allow-Headers");
	    response.setHeader("true", "Access-Control-Allow-Credentials");
		response.setHeader("no-cache","Pragma");
		response.setHeader("max-age=0, no-cache, no-store","Cache-Control");
	
		if(error){
			response.setHeader("400", "status-code");
		}
		
		 System.out.println("here");
		return response;
	}
	
	
	public static WOResponse responseWithContent(NSData content, String contentType, boolean error, WOResponse response) {
		//Response response = new WOResponse();
		
		response.setContent(content);
		response.setContentEncoding("UTF8");
		response.setHeader("application/" + contentType + "; charset=UTF-8" , "Content-Type");
		response.setHeader("Connection", "keep-alive");
	    response.setHeader(Integer.toString(content.length()), "content-length");
	    response.setHeader(PROTOCOL+"://" + DOMAIN, "Access-Control-Allow-Origin");
	    response.setHeader("true", "Access-Control-Allow-Credentials");
	    response.setHeader("X-Requested-With", "Access-Control-Allow-Headers");
	
		if(error){
			response.setHeader("400", "status-code");
		}
		
		return response;
	}
	
	/*
	 * takes any simple dictionary and converts and returns it as a JSONResponse using existing WOResponse
	 */
	
	public static WOResponse dictionaryToJSONResponse(NSDictionary<String,Integer> values, WOResponse r){
		
		NSArray<String> keys = values.allKeys();
		String response = "{ \"stats\": {";
		
		for(int i = 0; i <  keys.count(); i++) {
			
			String aKey = (String)keys.objectAtIndex(i);
			response = response +"\"" + aKey + "\":" + "\"" + (Integer)values.valueForKey(aKey) + "\"";
			if(i != (keys.count() -1)) {
				response = response + ",";
			}
		}
		response = response + " } }";
		return JSONHelper.jsonResponseWithContentString(response, r);
	}
	
	
	
	/*
	 * takes any simple dictionary and converts and returns it as a JSONResponse
	 */
	
public static WOResponse dictionaryToJSONResponse(NSDictionary<String,?> values){
		
		NSArray<String> keys = values.allKeys();
		String response = "[";
		
		for(int i = 0; i <  keys.count(); i++) {
			
			String aKey = (String)keys.objectAtIndex(i);
			response = response + "{\""+ aKey + "\": " + "\"" + (Object)values.valueForKey(aKey) + "\"}";
			if(i != (keys.count() -1)) {
				response = response + ",";
			}
		}
		response = response + "]";
		return JSONHelper.jsonResponseWithContentString(response);
	}
	
/*
 * takes any simple dictionary and converts and returns it as a JSONResponse
 */

public static WOResponse arrayToJSONResponse(NSArray<NSDictionary<String, String>> immutableArray) {
	
	String response = "[";

	
	for(NSDictionary<String, String> dictionary : immutableArray){
		
		NSArray<String> keys = dictionary.allKeys();

				for(int i = 0; i <  keys.count(); i++) {
					
					String aKey = (String)keys.objectAtIndex(i);
					response = response + "{\""+ aKey + "\": " + "\"" + (Object)dictionary.valueForKey(aKey) + "\"}";
					
					if(!immutableArray.lastObject().equals(dictionary)) {
						response = response + ",";
					}
				}
	}
	
	response = response + "]";
	System.out.println("Response " + response);

	
	return JSONHelper.jsonResponseWithContentString(response);
}




	
}
