package shopping.processing.response.json;

import com.webobjects.appserver.WOResponse;

public class JsonResponse extends JSONHelper {

	public JsonResponse() {
	
	}

	private static WOResponse BasicResponse() {
		
		WOResponse response = new WOResponse();
	    response.setHeader("application/json", "content-type");
		return response;
	}
	
	private static WOResponse JsonResponse(String json){
		
		WOResponse response = BasicResponse();
	    response.setHeader(Integer.toString(json.length()), "content-length");
	    response.setContent(json);
	    
	    return response;
	}
	
	
	public static WOResponse Response(String json){
		return JsonResponse(json);
	} 
	
	
}
