package shopping.processing.response.json;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import shopping.processing.app.DirectAction;
import shopping.processing.database.CustomerConstants;
import shopping.processing.database.Noty;

public class JsonOutput {

    private static Logger log = Logger.getLogger(JsonOutput.class);

	
	public JsonOutput() {
		super();
	}
	
	
	public static NSDictionary<Object, Object> JsonStringToDictionaryForNotification(String jsonString, Noty CODE ) throws Exception {
		
		NSDictionary<Object, Object> jsonDictionary = null;
		
		try {
			JsonParser parser = new JsonParser();
			JsonObject jobj = parser.parse(jsonString).getAsJsonObject();
			
			switch(CODE){
			
			case SOCIAL_PROOF : {
				
				String customer = jobj.get(CustomerConstants.NOTIFICATION_CUSTOMER).getAsString();
				String location = jobj.get(CustomerConstants.NOTIFICATION_LOCATION).getAsString();
				String order = jobj.get(CustomerConstants.NOTIFICATION_ORDER).getAsString();
				jsonDictionary = new NSDictionary<Object, Object>(new Object[] {customer, location, order}, new Object[]{CustomerConstants.NOTIFICATION_CUSTOMER, CustomerConstants.NOTIFICATION_LOCATION, CustomerConstants.NOTIFICATION_ORDER });
		
				break;
			}
			
			default: {
				
			}
			
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Json Not Created");
		}
		
		return jsonDictionary;

	}
	
	public static NSDictionary<Object, Object> JsonStringToDictionary(String jsonString, boolean includeStripe ) throws Exception {
		
		NSDictionary<Object, Object> jsonDictionary = null;
		
		try {
			JsonParser parser = new JsonParser();
			JsonObject jobj = parser.parse(jsonString).getAsJsonObject();
			
			if(!includeStripe) {
				String cost = jobj.get(CustomerConstants.COST).getAsString();
				String item = jobj.get(CustomerConstants.ITEM).getAsString();
				String shipping = jobj.get(CustomerConstants.SHIPPING).getAsString();
				jsonDictionary = new NSDictionary<Object, Object>(new Object[] {item, cost, shipping}, new Object[]{CustomerConstants.ITEM, CustomerConstants.COST, CustomerConstants.SHIPPING });
			
			}else {
				
				String token = jobj.get(CustomerConstants.TOKEN).getAsString();
				String cost = jobj.get(CustomerConstants.COST).getAsString();
				String item = jobj.get(CustomerConstants.ITEM).getAsString();
				String shipping = jobj.get(CustomerConstants.SHIPPING).getAsString();

				jsonDictionary = new NSDictionary<Object, Object>(new Object[] {item, cost, token, shipping }, new Object[]{CustomerConstants.ITEM, CustomerConstants.COST, CustomerConstants.TOKEN, CustomerConstants.SHIPPING });
				
			}
		
		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Json Not Created");
		}
	return jsonDictionary;
	}
	
	
	
	public static NSDictionary<Object, Object> JsonValueFromJsonNotificationString(String jsonString, Noty CODE) throws Exception {
		
		return JsonStringToDictionaryForNotification(jsonString, CODE);
	}
	
	public static NSDictionary<Object, Object> JsonValueFromJsonString(String jsonString, boolean includeStripe) throws Exception {
		
		return JsonStringToDictionary(jsonString, includeStripe);
	}
	
	
	
	
	public static String JsonNotificationForNoty(boolean status, NSArray<NSDictionary<Object, Object>> notifications){
		
		String state = status ? "OK" : "FAIL";

		Gson gson = new Gson();
		ArrayList<NSDictionary<Object, Object>> objs = new ArrayList<NSDictionary<Object, Object>>();
		
		if(!status) {
			String message = CustomerConstants.FAIL;
			NSDictionary<Object, Object> dictionary = new NSDictionary<Object, Object>(new String[] {state, message}, new String[] {"statusJS", "message"});	
			objs.add(dictionary);
			return gson.toJson(objs);
		}
		
		

		ArrayList<NSDictionary<Object, Object>> objsOrders = new ArrayList<NSDictionary<Object, Object>>();

		for(NSDictionary<Object, Object> notification : notifications){
			
			String customer = (String)notification.objectForKey(CustomerConstants.NOTIFICATION_CUSTOMER);
			String location = (String)notification.objectForKey(CustomerConstants.NOTIFICATION_LOCATION);
			String order = (String)notification.objectForKey(CustomerConstants.NOTIFICATION_ORDER);

			NSDictionary<Object, Object> dictionary = new NSDictionary<Object, Object>(
					new String[] {customer, location, order}, 
					new String[] {CustomerConstants.NOTIFICATION_CUSTOMER, CustomerConstants.NOTIFICATION_LOCATION, CustomerConstants.NOTIFICATION_ORDER});	
			objsOrders.add(dictionary);
				
		}
		
		NSDictionary<Object, Object> stateDictionary = new NSDictionary<Object, Object>(new Object[] {state , objsOrders}, new String[] {"statusJS", "result"});	
		
		objs.add(stateDictionary);
		
		
		return gson.toJson(objs);
		
	}
	
	
	
	
	public static String JsonStatusWithAmount(boolean status, String amount){
		
		String state = "OK";
		if(!status){
			state = "FAIL";
		}
		
		Gson gson = new Gson();
		ArrayList<NSDictionary<String, String>> objs = new ArrayList<NSDictionary<String, String>>();
		NSDictionary<String, String> dictionary = new NSDictionary<String, String>(new String[] {state, amount}, new String[] {"statusJS", "amount"});
		objs.add(dictionary);
		
		return gson.toJson(objs);
	}
	
	
	
	
	public static String JsonNotification(boolean status, String message){
		
		String state = status ? "OK" : "FAIL";

		Gson gson = new Gson();
		ArrayList<NSDictionary<String, String>> objs = new ArrayList<NSDictionary<String, String>>();
		
		NSDictionary<String, String> dictionary = new NSDictionary<String, String>(new String[] {state, message}, new String[] {"statusJS", "message"});
		
		objs.add(dictionary);
		return gson.toJson(objs);
		
	}
	
	public static String JsonStatusWithAddress(boolean status, String json){
		
		String state = "OK";
		Gson gson = new Gson();
		
		if(!status){
			state = "FAIL";
		
			ArrayList<NSDictionary<String, String>> objs = new ArrayList<NSDictionary<String, String>>();
			NSDictionary<String, String> dictionary = new NSDictionary<String, String>(new String[] {state, json}, new String[] {"statusJS", "result"});
			objs.add(dictionary);
			return gson.toJson(objs);
		
		} else {
	
		    JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(json);
		    JsonObject jsonObject = jsonElement.getAsJsonObject();
		    jsonObject.addProperty("status", state);
		    return jsonObject.toString();

		}
		
	}

}
