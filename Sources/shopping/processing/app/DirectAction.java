package shopping.processing.app;

import java.math.BigDecimal;

import javax.servlet.http.Cookie;

import org.apache.log4j.Logger;


import shopping.processing.components.InitComponent;
import shopping.processing.components.Main;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver.WOSession;
import com.webobjects.appserver.WOSessionStore;
import com.webobjects.foundation.NSArray;
import er.extensions.appserver.ERXDirectAction;
import er.extensions.foundation.ERXStringUtilities;




public class DirectAction extends ERXDirectAction {
	//https://davidleber.net/category/webobjects/using-directactions/
   // private static Logger log = Logger.getLogger(DirectAction.class);
   
	
	public DirectAction(WORequest request) {
		super(request);
	}


	public Application application() {
		return (Application)WOApplication.application();
	}
	
	@Override
	public Session session() {
		return (Session)super.session();
	}
	
	
	@Override
	public WOActionResults defaultAction() {
		//this.forceSession(); $$$
		return pageWithName(Main.class); // $$$$
	}
	

	public String getSessionId(){
		return this.session().sessionID();
	}
	
	public void forceSession(){
		existingSession();
		this.session().sessionID();
	}
	
	private String cookieString(){
		return this.context().request().cookieValueForKey("wosid");	
	}
	
	private boolean cookiesEnabled(){
		String cookie = this.context().request().cookieValueForKey("wosid");		
		if(ERXStringUtilities.stringIsNullOrEmpty(cookie)){
			return false;
		}
		return true;
	}
	
@Override
	public WOActionResults performActionNamed(String actionName) {
		return super.performActionNamed(actionName);
	}


	private boolean hasSessionForCookie(String cookie){
		WOSessionStore st = application().sessionStore();
		WOSession session = st.restoreSessionWithID(cookie,request());
		return (session != null);
	}
	
	

	
	private NSArray<WOCookie> cookiesForResponse(){
		
		
	//	log.info("########");
	//	log.info("Here is the wosid " + cookieString());
	//	log.info("########");
		
		NSArray<WOCookie> cookies = NSArray.emptyArray();

		WOResponse response = null;

		//log.info("Cookies Enabled  " + cookiesEnabled());

		
		if(!cookiesEnabled()){ // checks if there is a wosid in the request
			this.forceSession();
			response = pageWithName(InitComponent.class).generateResponse();
			cookies = response.cookies();
		//	log.info("########");
		//	log.info("Cookies in side  " + cookies);
		//	log.info("########");

		}else {
			
			WOSessionStore st = application().sessionStore();
			WOSession session = st.restoreSessionWithID(cookieString(), request());
			
		//	log.info("########");
		//	log.info("This is the session  " + session);
		//	log.info("########");
			
			if(session == null){
				
				// stale cookie
				this.forceSession();
				
				response = pageWithName(InitComponent.class).generateResponse();
				cookies = response.cookies();
		//		log.info("########");
		//		log.info("No session lets get some cookies  " + cookies);
		//		log.info("########");
			
			}
		//	log.info("########");
		//	log.info("There was a session so lets see cookies  " + cookies);
		//	log.info("########");
			
		}
		
		return cookies;
	}
	
}
	

	



	
	