package shopping.processing.app;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXSession;
import er.extensions.concurrency.ERXExecutorService;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXStringUtilities;
import shopping.entity.eo.FPSChargeDetail;
import shopping.entity.eo.FPSOfferTracking;
import shopping.entity.eo.FPSPerson;
import shopping.entity.eo.Person;
import shopping.processing.util.NumberFormat;

public class Session extends ERXSession {
	
	
    private static Logger log = Logger.getLogger(Session.class);

	private static final long serialVersionUID = 1L;
	private String COOKIE_PATH = "/";
	

	public Session() {
		this.setStoresIDsInURLs(false);
		this.setStoresIDsInCookies(true);
	}
	
	@Override
	public Application application() {
		return (Application)super.application();
	}
	
	// COOKIE
	
	public String domainForIDCookies()
	 {
	    return this.COOKIE_PATH;
	 }
	
	@Override
	 public boolean useSecureSessionCookies() {
		  return true; // $$$
	  }


}
