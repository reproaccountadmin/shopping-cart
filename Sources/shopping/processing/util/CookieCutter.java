package shopping.processing.util;

import com.webobjects.appserver.WOCookie;
import com.webobjects.foundation.NSTimestamp;

public class CookieCutter {

	public CookieCutter() {
		// TODO Auto-generated constructor stub
	}

	

    public static WOCookie cookieForNameValueAndDomainZeroDate(String domain, String name, String value){
        String myKey = name;
        String myValue = value;
        WOCookie newCookie = new WOCookie(myKey, myValue);
        NSTimestamp myNSTimestamp = new NSTimestamp(1970, 0, 0, 0, 0, 0, null);
        newCookie.setExpires(myNSTimestamp);
        newCookie.setDomain(domain);
        newCookie.setPath("/");
        newCookie.setIsSecure(true);
        
        return newCookie;
    }
    
	
    public static WOCookie secureCookieForNameAndValue(String name, String value){
        String myKey = name;
        String myValue = value;
        WOCookie newCookie = new WOCookie(myKey, myValue);
        NSTimestamp myNSTimestamp = new NSTimestamp(2045, 12, 31, 0, 0, 0, null);
        newCookie.setExpires(myNSTimestamp);
        newCookie.setPath("/");
        newCookie.setIsSecure(true);
        
        return newCookie;
    }
    

    public static WOCookie cookieForNameAndValue(String name, String value){
        String myKey = name;
        String myValue = value;
        WOCookie newCookie = new WOCookie(myKey, myValue);
        NSTimestamp myNSTimestamp = new NSTimestamp(2045, 12, 31, 0, 0, 0, null);
        newCookie.setExpires(myNSTimestamp);
        newCookie.setPath("/");
        
        
        return newCookie;
    }
    
    
 
    
	
	
    public static WOCookie cookieForNameAndValueZeroDate(String name, String value){
        String myKey = name;
        String myValue = value;
        WOCookie newCookie = new WOCookie(myKey, myValue);
        NSTimestamp myNSTimestamp = new NSTimestamp(1970, 0, 0, 0, 0, 0, null);
        newCookie.setExpires(myNSTimestamp);
        newCookie.setPath("/");
        
        return newCookie;
    }
	
	
}
