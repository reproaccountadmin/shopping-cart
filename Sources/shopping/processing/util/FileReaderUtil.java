package shopping.processing.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderUtil {

	private String path;
	private String fileName;
	
	public FileReaderUtil(String path, String fileName) {

		this.path = path;
		this.fileName = fileName;
	}
	
	
	public String fileContents() throws Exception{
		
		// The name of the file to open.

        // This will reference one line at a time
        String line = null;
        StringBuffer buffer = new StringBuffer();

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(this.path + "/" + this.fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	buffer.append(line);
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
        
        return buffer.length() > 0 ? buffer.toString() : null;
        
    }
		
	}
	
	
	
