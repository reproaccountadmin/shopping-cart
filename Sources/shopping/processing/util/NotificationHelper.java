package shopping.processing.util;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class NotificationHelper {

	public NotificationHelper() {
		// TODO Auto-generated constructor stub
	}
	
	
	public static NSDictionary<Object, Object> notificationFromMessage(Boolean status, String message){
		
		NSMutableDictionary<Object, Object> notification = new NSMutableDictionary<Object, Object>();
		
		notification.setObjectForKey(status, "status");
		notification.setObjectForKey(message, "message");
		
		return notification.immutableClone();
	}

}
