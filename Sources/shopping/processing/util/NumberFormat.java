package shopping.processing.util;

import com.ibm.icu.math.BigDecimal;

import er.extensions.foundation.ERXStringUtilities;

public class NumberFormat {

	public NumberFormat() {
		// TODO Auto-generated constructor stub
	}
	
/*
 * 		if(!ERXStringUtilities.stringIsNullOrEmpty(inString)) {
	
			try{
				
				Double amount = (new Double(inString));
				
				if(amount > 0.00){
				
					_amount_total = BigDecimal.valueOf(amount).toString();
				}
			
			}catch(NumberFormatException e){
				_amount_total = "0";
			}

		
		}else {
			_amount_total = "0";
		}
		
		return _amount_total;
 * */
	
	public static String subTotalFullCostLessShipping(String amount, String shipping) {
		
		String _amount = "0";
		
		try{
			
			Double amount1 = new Double(amount);
			Double amount2 = new Double(shipping);
			
			
			if(amount1.compareTo(amount2) > 0){
				Double sum = (amount1 - amount2);
				_amount = (BigDecimal.valueOf(sum).setScale(0)).toString();
			}else {
				
				_amount = "0";
			}
			
			
			
		}catch(NumberFormatException e){
		
			_amount = "0.00";
		}
		
		return _amount;
		
	}
	
	
	public static String AddCentsToCents(String _amount_total, String _initialAmount) {
		
		String _amount = "0";
		try{
			
			Double amount1 = new Double(_amount_total);
			Double amount2 = new Double(_initialAmount);
			
			Double sum = amount1 + amount2;
			
			_amount = (BigDecimal.valueOf(sum).setScale(0)).toString();
			
		}catch(NumberFormatException e){
			_amount = "0.00";
		}
		return _amount;
	}
	
	
	public static String CentsToDecimalString(String _amount_total) {
		
		try{
			
			_amount_total = BigDecimal.valueOf(new Double(_amount_total) / 100).setScale(2, BigDecimal.ROUND_DOWN).toString();
			
		}catch(NumberFormatException e){
			_amount_total = "0.00";
		}
		
		return _amount_total;
		
	}

}
