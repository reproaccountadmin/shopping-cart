package shopping.processing.components;

import com.webobjects.appserver.WOContext;

public class PaymentNotification extends GeneralNotification {
    public PaymentNotification(WOContext context) {
        super(context);
    }
}