package shopping.processing.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;

public class ErrorPage extends BaseComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NSDictionary<Object, Object> notification;

	public ErrorPage(WOContext context) {
        super(context);
    }
	
	

	/**
	 * @return the notification
	 */
	public NSDictionary<Object, Object> notification() {
		return notification;
	}


	/**
	 * @param notification the notification to set
	 */
	public void setNotification(NSDictionary<Object, Object> notification) {
		this.notification = notification;
	}
	

	
	public void sleep()  {
		super.sleep();
		notification = null;
	}

}