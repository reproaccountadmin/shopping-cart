package shopping.processing.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class GeneralNotification extends BaseComponent {
   
	protected NSDictionary<Object, Object> _notification;

    public GeneralNotification(WOContext context) {
        super(context);
    }

    public NSDictionary<Object, Object> notification()
    {
        return _notification;
    }
    public void setNotification(NSDictionary<Object, Object> newNotification)
    {
    	//System.out.println("Setting Notification " + newNotification);
        _notification = newNotification;
    }
    
    public void sleep(){
        super.sleep();
      setNotification(null);
    }

	
	
}