package shopping.processing.components;


import org.apache.log4j.Logger;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;

import billing.payment.components.CompositeMail;
import billing.payment.mail.MailManager;
import billing.payment.mail.MailerInitiator;

import com.webobjects.appserver.WOActionResults;

public class Main extends BaseComponent {
	
    private static Logger log = Logger.getLogger(Main.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String merchantID;
	private String clientID;

	private NSDictionary<Object, Object> notification;

	public Main(WOContext context) {
		super(context);
	}
	
	

	/**
	 * @return the notification
	 */
	public NSDictionary<Object, Object> notification() {
		return notification;
	}


	/**
	 * @param notification the notification to set
	 */
	public void setNotification(NSDictionary<Object, Object> notification) {
		this.notification = notification;
	}
	

	
	public void sleep()  {
		super.sleep();
		notification = null;
	}

	
}
