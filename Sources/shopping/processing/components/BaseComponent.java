package shopping.processing.components;

import com.webobjects.appserver.WOContext;

import er.extensions.components.ERXComponent;
import shopping.processing.app.Application;
import shopping.processing.app.Session;

public class BaseComponent extends ERXComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BaseComponent(WOContext context) {
		super(context);
	}
	
	@Override
	public Application application() {
		return (Application)super.application();
	}
	
	@Override
	public Session session() {
		return (Session)super.session();
	}
}
