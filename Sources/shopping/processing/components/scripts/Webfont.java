package shopping.processing.components.scripts;

import java.util.Iterator;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import shopping.processing.components.BaseComponent;

public class Webfont extends ScriptBase {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private NSArray<String> fonts;

	
	public Webfont(WOContext context) {
        super(context);
        fonts = new NSArray<String>(new String[] {"Ubuntu:400,700", "Montserrat:400,900"});
    }
	

	
	public String webFontSrc(){
		
		Iterator<String> iterator = this.fonts.iterator();
		String str = "";
		while(iterator.hasNext()){
			str += "'";
			str += iterator.next();
			str += "'";
			if(iterator.hasNext()){
				str += ",";
			}
		}
		return  "WebFont.load({google:{families:[" +  str  + "]}, timeout: 3000, events: false });";
	}
	/*
    public boolean isStateless() {
 	   return true;
 	 } 
	

	public String scriptStr() {    	
    	return domain() + path()  + resourceSrc();	
    }
	


    public String path() {
    	//	return _path;
    		return (String)valueForBinding("path");
    	}

    	public void setPath(String path) {
    		//this._path = path;
    		setValueForBinding(path, "path");
    	}
    	
    	public String domain() {
    		return (String)valueForBinding("domain");
    	}

    	public void setDomain(String domain) {
    		setValueForBinding(domain, "domain");
    	}

    	public String resourceSrc() {
    		return (String)valueForBinding("resourceSrc");
    	}

    	public void setResourceSrc(String resource) {
    		setValueForBinding(resource, "resourceSrc");
    	}
*/

    
    
    
}