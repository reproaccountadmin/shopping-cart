package shopping.processing.components;

import com.webobjects.appserver.WOContext;

public class Navigation extends BaseComponent {
    
	
	public Navigation(WOContext context) {
        super(context);
    }
    
	
    public boolean isStateless(){
    	return true;
    }
	
}