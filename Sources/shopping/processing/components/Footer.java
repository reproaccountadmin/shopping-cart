package shopping.processing.components;

import com.ibm.icu.util.GregorianCalendar;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class Footer extends BaseComponent {
    public Footer(WOContext context) {
        super(context);
    }
    
    public boolean isStateless(){
    	return true;
    }
    
    
    
    public String lastTwoDigitsOfMonth() {
		return StringForCurrentYearLastTwoDigits();
	}
    
	
    private static String StringForCurrentYearLastTwoDigits(){
        
        NSTimestamp date = new NSTimestamp();
        GregorianCalendar newCalendar = new GregorianCalendar();
        newCalendar.setTime(date);
        int myyear = newCalendar.get(GregorianCalendar.YEAR);      
        
        myyear = (myyear - 2000);
        
        return new Integer(myyear).toString();
    }
}