package shopping.processing.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WOResponse;

import shopping.processing.util.CookieCutter;

public class FPSThankYouPage extends BaseComponent {
   
	
	public FPSThankYouPage(WOContext context) {
        super(context);
    }
    
	
    public void appendToResponse(WOResponse res,  WOContext con){
        
    	WOCookie amazon_Login_accessToken = CookieCutter.cookieForNameAndValueZeroDate("amazon_Login_accessToken", "");
        res.addCookie(amazon_Login_accessToken);
        
    	WOCookie amazon_Login_state_cache = CookieCutter.cookieForNameAndValueZeroDate("amazon_Login_state_cache", "");
        res.addCookie(amazon_Login_state_cache);
        
       
    	WOCookie wosid = CookieCutter.cookieForNameAndValueZeroDate("wosid", "");
        res.addCookie(wosid);
           
        
    	WOCookie amazon = CookieCutter.cookieForNameValueAndDomainZeroDate("payments-amazon.com", "*", "");
        res.addCookie(amazon);
        
        super.appendToResponse(res, con);
       
        
    }
    
    
    public void sleep(){
    	super.sleep();
    	this.session().terminate();
    }
}