package shopping.processing.components;

import com.webobjects.appserver.WOContext;

public class Meta extends BaseComponent {
    public Meta(WOContext context) {
        super(context);
    }
    
    public boolean isStateless(){
    	return true;
    }
}