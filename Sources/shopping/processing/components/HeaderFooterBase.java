package shopping.processing.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;

import shopping.processing.components.BaseComponent;

public class HeaderFooterBase extends BaseComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private NSDictionary<Object, Object> notification;
	
	public HeaderFooterBase(WOContext aContext) {
		super(aContext);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the title
	 */
	public String title() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	

	/**
	 * @return the notification
	 */
	public NSDictionary<Object, Object> notification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(NSDictionary<Object, Object> notification) {
		this.notification = notification;
	}
	

}
