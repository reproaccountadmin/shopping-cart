package shopping.processing.components.stylesheet;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class StyleSheetBase extends WOComponent {

	

	private String domain;
	private String path;
	private String resourceSrc;
	
	public StyleSheetBase(WOContext aContext) {
		super(aContext);
		// TODO Auto-generated constructor stub
	}
	
	
	public void reset(){
		domain = null;
		path = null;
		resourceSrc = null;
	}
    public boolean isStateless() {
  	   return true;
  	 } 
    
    
    public String scriptStr() {    	
    	return domain() + path()  + resourceSrc();	
    }
    
    
	
    
	public String path() {
	//	return _path;
		return (String)valueForBinding("path");
	}

	public void setPath(String path) {
		//this._path = path;
		setValueForBinding(path, "path");
	}
	
	public String domain() {
		return (String)valueForBinding("domain");
	}

	public void setDomain(String domain) {
		setValueForBinding(domain, "domain");
	}

	public String resourceSrc() {
		return (String)valueForBinding("resourceSrc");
	}

	public void setResourceSrc(String resource) {
		setValueForBinding(resource, "resourceSrc");
	}
	

}
