package shopping.processing.components;

import com.webobjects.appserver.WOContext;

public class FPSThankYouIntroText extends BaseComponent {
    public FPSThankYouIntroText(WOContext context) {
        super(context);
    }
    
    private String firstName;
    
    public boolean isStateless(){
    	return true;
    }
    
    public void reset(){
    	this.firstName = null;
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}