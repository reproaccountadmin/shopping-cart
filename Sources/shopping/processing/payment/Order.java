package shopping.processing.payment;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import billing.payment.components.CompositeMail;
import billing.payment.components.OuterTemplate;
import billing.payment.constant.Constants;
import billing.payment.execution.ExecutePdf;
import billing.payment.mail.MailManager;
import billing.payment.order.CustomerOrder;
import er.extensions.concurrency.ERXExecutorService;
import httpcom.connect.post.SendPost;
import httpcom.constant.MauticConstant;
import shopping.entity.eo.FPSChargeDetail;
import shopping.entity.eo.FPSCustomerOrder;
import shopping.entity.eo.FPSFreePlusShippingAddress;
import shopping.entity.eo.FPSPerson;
import shopping.processing.database.CustomerConstants;
import shopping.processing.database.DecodeCustomerFormResults;
import shopping.processing.database.EnterpriseObjectsManagement;

public final class Order {

	private Order() {
		// TODO Auto-generated constructor stub
	}
	
	
	public synchronized static void CreateInvoice(EOEditingContext _ec, EOGlobalID  chargeGlobalId, EOGlobalID  personGlobalId, String subTotal, String shipping, String total, NSDictionary<Object, Object> result, OuterTemplate invoiceTempate) throws Exception{
		

			String _item = (String)result.objectForKey(CustomerConstants.ITEM);
			_item = DecodeCustomerFormResults.ItemWithSpacesFromUnderscore(_item);
			
		
			NSMutableDictionary<Object, Object> order = DecodeCustomerFormResults.CustomerDictionaryFromPersonAndShipping(_ec, personGlobalId);
			order = DecodeCustomerFormResults.CustomerDictionaryFromOrderDetails(order, subTotal, shipping, total);
			
		 	NSMutableArray<NSDictionary<Object, Object>> _items = new NSMutableArray<NSDictionary<Object, Object>>();
	 		NSMutableDictionary<Object, Object> lineItem = new NSMutableDictionary<Object, Object>();
	 		
	 		String item = (String)result.objectForKey(CustomerConstants.ITEM);
	 		String emailAddress = (String)result.objectForKey(Constants.EMAIL);
	 		String subject = (String)result.objectForKey(Constants.SUBJECT);

	 		
	 		lineItem.setObjectForKey( _item, Constants.ITEM);
	 		lineItem.setObjectForKey("1", Constants.QUANTITY);
	 		lineItem.setObjectForKey(subTotal, Constants.UNIT_PRICE);
	 		lineItem.setObjectForKey(total, Constants.FULL_PRICE);
	 		_items.addObject(lineItem.immutableClone());

	 		order.setObjectForKey(_items, Constants.ITEMS);
	 		order.setObjectForKey(emailAddress, Constants.EMAIL);
	 		order.setObjectForKey(subject, Constants.SUBJECT);

	 		FPSPerson person = (FPSPerson)_ec.faultForGlobalID(personGlobalId, _ec);
	 		CustomerOrder cusOrder = new CustomerOrder(order);
	 		cusOrder.setItemForContent(_item);
	 		cusOrder.setFirstName(person.firstName());
	 		invoiceTempate.setCustomerOrder(cusOrder);
			String filePath = createAndMailInvoice(cusOrder, invoiceTempate);
			
			if(!filePath.equals(Constants.FAIL) && person != null){
				FPSCustomerOrder customerOrder = EnterpriseObjectsManagement.CreateCustomerOrderWithDetails(_ec, filePath, subTotal, shipping, total, _item);		
				person.addToFpsCustomerOrders(customerOrder);
				customerOrder.setFpsPerson(person);
				FPSChargeDetail chargeDetail = (FPSChargeDetail)_ec.faultForGlobalID(chargeGlobalId, _ec);
				chargeDetail.setFpsCustomerOrder(customerOrder);
				EnterpriseObjectsManagement.SaveAllEnterpriseObjects(_ec);

			}
			
			
	}
	
	
	
	public synchronized static String createAndMailInvoice(CustomerOrder order, OuterTemplate invoiceTempate) throws Exception{
		
		//OuterTemplate invoiceTempate = pageWithName(OuterTemplate.class);
 		//invoiceTempate.setCustomerOrder(order);
		String bodyContent = order.getItemForContent();
		
		WOContext context = invoiceTempate.context();
 		System.out.println("in");
 		ExecutePdf task = new ExecutePdf(invoiceTempate);
 		task.run();
 		String filePath = task.path();
 		
 		CompositeMail emailComponent = new MailManager(context).getCompositeMail();
 		emailComponent.setPersonName(order.getFirstName());
 		emailComponent.setBodyContent(bodyContent);
 		
 		emailComponent.setLayout(new NSDictionary<Object, Object>( new Object[] { Boolean.TRUE }, new Object[]{ CustomerConstants.MY_BUDDI } ) );
 	
 		try {
 			String emailfilePath = Constants.WEBSERVER_BASE_PATH + filePath; 
			MailManager.SendHTMLEmailWithAttachment(order.getSubject(), order.getEmail(), emailComponent, emailfilePath);
		} catch (Exception e) {
			filePath = CustomerConstants.FAIL;
			e.printStackTrace();
		}
 		
 		return filePath;
		
	}
	
	
	public synchronized static void SendToMarketingAutomation(EOEditingContext _ec, EOGlobalID personGlobalId){
		
 			FPSPerson _fpsPerson = (FPSPerson)_ec.faultForGlobalID(personGlobalId, _ec);

		
 			NSMutableDictionary<Object, Object> formDictionary = new NSMutableDictionary<Object, Object>();
	        formDictionary.setObjectForKey(_fpsPerson.firstName(), MauticConstant.FIRST_NAME);
	        formDictionary.setObjectForKey(_fpsPerson.lastName(), MauticConstant.LAST_NAME);
	        formDictionary.setObjectForKey(_fpsPerson.emailAddress(), MauticConstant.EMAIL);
	        
	        FPSFreePlusShippingAddress _address = _fpsPerson.fpsShippingAddress();
	        
	        formDictionary.setObjectForKey(_address.addressOne(), MauticConstant.ADDRESS_1);
	        formDictionary.setObjectForKey(_address.city(), MauticConstant.CITY);
	        formDictionary.setObjectForKey(_address.state(), MauticConstant.STATE);
	        formDictionary.setObjectForKey(_address.zipcode(), MauticConstant.ZIPCODE);
	        formDictionary.setObjectForKey(_address.country(), MauticConstant.COUNTRY);
	        formDictionary.setObjectForKey("buyer", MauticConstant.FORM_NAME); // buyer
	        formDictionary.setObjectForKey("6", MauticConstant.FORM_ID); // 6
	        formDictionary.setObjectForKey("https://app.mybuddi.co/form/submit", MauticConstant.MAUTIC_FORM_URL); // "https://app.mybuddi.co/form/submit";
			
		
	        SendPost sendPostToMautic = new SendPost(formDictionary.immutableClone());
	        ERXExecutorService.executorService().submit(sendPostToMautic);
	}
	
	
	
	
	

}
